 var Bicicleta = require('../../models/bicicleta');

 exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
 }

 exports.bicicleta_create = function(req,res){

   const bici = new Bicicleta();
   bici.code = req.body.id;
   bici.color = req.body.color;
   bici.modelo = req.body.modelo;
   bici.ubicacion = [req.body.lat,req.body.lng];

   Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });  

 }

 exports.bicicleta_delete = function(req,res){
    Bicicleta.removeByCode(req.body.id);
    res.status(204).send();
 }

 exports.bicicleta_update = function(req,res){

    var bici = Bicicleta.findByCode(req.params.id, function(err,bici){
         if(err) console.log(`El error es ${err}`);
         
         bici.color = req.body.id;
         bici.color = req.body.color;
         bici.modelo = req.body.modelo;
         bici.ubicacion = [req.body.lat,req.body.lng];

          res.status(200).json({
            bicicleta: bici
         });

         //console.log(`respuesta es ${JSON.stringify(bici)}`);
    });
   

 }