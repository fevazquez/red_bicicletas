var mymap = L.map('main_map').setView([-34.6012424, -58.3861497], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoiam9zZW1pbGxvbmFyaW8xOCIsImEiOiJja2hzNHJkNWowb2YyMnVtanRrN3Yxa3B2In0.XOfWbnE37UQIN8W3T5ndTQ'
}).addTo(mymap);

// L.marker([-34.6012424,-58.3861497]).addTo(mymap);
// L.marker([-34.596932,-58.3808287]).addTo(mymap);
// L.marker([-34.599564,-58.3778777]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(mymap);
        });
    }
});

