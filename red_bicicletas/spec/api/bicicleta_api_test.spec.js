var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API',() => {

    beforeAll(function(done){        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.createConnection(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true});
        //mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});       
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',function(){
            console.log('We are connected to test database');
            done();
        });

    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            console.log("borrando");
            done();
        });
    });

    describe('GET BICICLETAS /', () =>{
        it('Status 200', (done) => {
            request.get(base_url,function(error, response,body){                               
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function(err,bicis){
                    //expect(bicis.length).toBe(0);
                    done();
                });                
            });            
        });
    });

    describe('POST BICICLETAS /create', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBIci = '{"id": 10, "color": "rojo", "modelo" : "urbana" , "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBIci
            }, function(error,response,body){                
                
                expect(response.statusCode).toBe(200);
                var biciCreated = JSON.parse(body).bicicleta;
                console.log(biciCreated);
                expect(biciCreated.color).toBe("rojo");
                expect(biciCreated.ubicacion[0]).toBe(-34);
                expect(biciCreated.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {

        it('Status 200',(done) => {
            var a = Bicicleta.createInstance(2, "negro","urbana", [-34.6541234,-58.3216845]);            
            Bicicleta.add(a, function(err,newBici){                
                var headers = {'content-type' : 'application/json'};
                var aModify = '{"id" :2 ,"color": "roja","modelo": "deportiva", "ubicacion": [-56.4542,-56.8754] }';
                request.post({
                    headers: headers,
                    url: base_url + '/2/update',
                    body: aModify
                },function(error,response,body){                    
                    expect(response.statusCode).toBe(200);                
                    var updated = JSON.parse(body).bicicleta;
                    expect(updated.modelo).toBe("deportiva");
                    expect(updated.color).toBe("roja");                    
                    done();
                });  
            });     
        });
   });

    describe('POST BICICLETAS /delete', () => {

        it('Status 204', (done) => {

            var previousLength = Bicicleta.allBicis.length;

            var a = Bicicleta.createInstance(5,"negro","urbana",[-34.6012424,-58.3861497]);           
            Bicicleta.add(a, function(err,newBici){
                var headers = {'content-type' : 'application/json'};
                var body = '{"id": 5}';

                request.delete({
                    headers: headers,
                    url: base_url + '/delete',
                    body: body
                },function(error,response,body){
                    expect(response.statusCode).toBe(204);                
                    expect(Bicicleta.allBicis.length).toBe(previousLength);
                    done();
                });
            });            
        });
    });
});